#!/usr/bin/env bash

touch buildArtifact.zip && rm buildArtifact.zip && zip -r buildArtifact.zip ./

aws s3  cp ./buildArtifact.zip  s3://pennidinh-code-assets/pennidinhadminserver-shouldreboot.zip

aws lambda update-function-code --function-name PenniDinhCentral-ShouldReboot --s3-bucket pennidinh-code-assets --s3-key pennidinhadminserver-shouldreboot.zip --region us-west-2
